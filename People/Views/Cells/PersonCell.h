//
//  PersonCell.h
//  People
//
//  Created by 4gt10 on 21.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phonesLabel;

@end
