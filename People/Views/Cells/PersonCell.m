//
//  PersonCell.m
//  People
//
//  Created by 4gt10 on 21.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "PersonCell.h"

@implementation PersonCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
