//
//  Person+CoreDataProperties.m
//  
//
//  Created by 4gt10 on 22.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person+CoreDataProperties.h"

@implementation Person (CoreDataProperties)

@dynamic name;
@dynamic avatarURL;
@dynamic phones;

@end
