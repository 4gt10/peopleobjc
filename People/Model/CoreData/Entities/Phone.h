//
//  Phone.h
//  
//
//  Created by 4gt10 on 22.01.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Person;

NS_ASSUME_NONNULL_BEGIN

@interface Phone : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Phone+CoreDataProperties.h"
