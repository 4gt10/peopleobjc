//
//  Phone+CoreDataProperties.h
//  
//
//  Created by 4gt10 on 22.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Phone.h"

NS_ASSUME_NONNULL_BEGIN

@interface Phone (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *number;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) Person *person;

@end

NS_ASSUME_NONNULL_END
