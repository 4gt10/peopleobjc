//
//  MockPersonGenerator.m
//  People
//
//  Created by 4gt10 on 21.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "MockPersonGenerator.h"
#import "AppDelegate.h"
#import "Person+CoreDataProperties.h"
#import "Phone+CoreDataProperties.h"

@interface MockPersonGenerator()

@property (nonatomic, copy) onCompletion completion;
@property (nonatomic, assign) BOOL shouldDelay;

// Mocks
@property (nonatomic, copy) NSArray *avatarURLs;
@property (nonatomic, copy) NSArray *names;
@property (nonatomic, copy) NSArray *phoneTypes;

@end

@implementation MockPersonGenerator

#pragma mark - Init

- (instancetype) init {
    if (self = [super init]) {
        self.avatarURLs = @[
                         @"https://pp.vk.me/c623920/v623920373/292fe/044quu11nzQ.jpg",
                         @"http://smexkartinka.ru/uploads/posts/2015-05/1432842014_av-425608.jpg",
                         @"http://cs627525.vk.me/v627525715/22a5/68aEoqqPn64.jpg",
                         @"http://smexkartinka.ru/uploads/posts/2015-03/1426228468_pjnldwtjtpm.jpg",
                         @"http://cs622429.vk.me/v622429899/d138/RxN6ixpIyZQ.jpg",
                         @"https://pp.vk.me/c412521/v412521668/80c/KRRcWfpLRWA.jpg",
                         @"http://what-messenger.com/uploads/posts/2015-10/1443925048_106987245_2953960_kung_fu_panda_by_juliafox90d4a031w.jpg",
                         @"http://inetklub.ru/avatarki/3D_koshka.jpg",
                         @"http://avatarko.at.ua/_ph/35/2/759495305.jpg",
                         @"http://www.fotoava.ru/images/90079.jpg",
                         @"http://xaxa-net.ru/uploads/posts/2013-06/1372087974_prikolnye-avatarki-4.jpg",
                         @"http://www.avatarworld.ru/avatarki/kontakt/avatarki-animal-dogs/avatars/43_devoted_eyes.jpg",
                         @"http://thumbs.dreamstime.com/t/cute-dachshund-puppies-close-up-white-background-36843320.jpg"
                         ];
        self.names = @[
                       @"Adam Smith",
                       @"John Doe",
                       @"Patrick Wayne",
                       @"Simona Lewis",
                       @"Barbara Carson",
                       @"Jeremy White",
                       @"Robert Kennedy",
                       @"Sam Black",
                       @"Tom Raddle"
                       ];
        self.phoneTypes = @[
                            @"Work",
                            @"Home",
                            @"Private",
                            @"Secret",
                            @"Top secret",
                            @"Really should be shared with nobody"
                            ];
    }
    
    return self;
}

#pragma mark - Interface

- (void) generatePeopleInBackgroundQueue: (NSUInteger) peopleCount
                                shouldDelay: (BOOL) shouldDelay
                              completion: (onCompletion) completion {
    self.completion = completion;
    self.shouldDelay = shouldDelay;
    
    // Main queue context
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *mainQueueContext = appDelegate.managedObjectContext;
    NSPersistentStoreCoordinator *mainQueueCoordinator = mainQueueContext.persistentStoreCoordinator;
    
    dispatch_queue_t addPeopleQueue = dispatch_queue_create("com.arturchernov.addPeopleQueue", 0);
    MockPersonGenerator *__weak weakSelf = self;
    dispatch_async(addPeopleQueue, ^{
        // Create a new managed object context for background queue
        // That will need to call NSManagedObjectContextDidSaveNotification
        NSManagedObjectContext *backgroundQueueContext = [[NSManagedObjectContext alloc] init];
        backgroundQueueContext.persistentStoreCoordinator = mainQueueCoordinator;
        
        // Register for context save changes notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(mergeChanges:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:backgroundQueueContext];
        
        for (int i = 0; i < peopleCount; i++) {
            // Create a person
            Person *person = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(Person.self)
                                                           inManagedObjectContext:backgroundQueueContext];
            person.avatarURL = weakSelf.avatarURLs[arc4random() % (weakSelf.avatarURLs.count - 1)];
            person.name = weakSelf.names[arc4random() % (weakSelf.names.count - 1)];
            // Create some phones
            const NSUInteger kMaxPhonesCount = 10;
            NSUInteger randomNumberOfPhones = arc4random() % kMaxPhonesCount;
            if (randomNumberOfPhones == 0) { randomNumberOfPhones++; } // should be at least one phone number
            NSMutableArray *usedPhoneTitles = [[NSMutableArray alloc] init]; // tracking used titles to avoid repeats
            for (int i = 0; i < randomNumberOfPhones; i++) {
                Phone *phone = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(Phone.self)
                                                             inManagedObjectContext:backgroundQueueContext];
                phone.title = weakSelf.phoneTypes[arc4random() % (weakSelf.phoneTypes.count - 1)];
                if (![usedPhoneTitles containsObject:phone.title]) {
                    phone.number = [self generateRandomPhoneNumber];
                    [person addPhonesObject:phone];
                    [usedPhoneTitles addObject:phone.title];
                }
            }
        }
        
        // Save a person in background queue context
        // That will call NSManagedObjectContextDidSaveNotification and merge changes in main queue
        NSError *error;
        [backgroundQueueContext save:&error];
    });
}

#pragma mark - Notification

- (void)mergeChanges:(NSNotification*)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSManagedObjectContext* context = ((AppDelegate*)[UIApplication sharedApplication].delegate).managedObjectContext;
        [context mergeChangesFromContextDidSaveNotification:notification];
        
        
        if (self.shouldDelay) {
            // Sleep for 5 seconds to emulate hard work
            const NSUInteger kSleepSecondsCount = 5;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, kSleepSecondsCount * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                self.completion();
            });
        } else {
            self.completion();
        }
    });
}

#pragma mark - Helpers

- (NSString*) generateRandomPhoneNumber {
    const NSUInteger kNumberLength = 10;
    const NSUInteger kUniqueNumberSymbolsCount = 10;
    
    NSString *phoneNumber = @"+7";
    for (int i = 0; i < kNumberLength; i++) {
        phoneNumber = [phoneNumber stringByAppendingString:[NSString stringWithFormat:@"%d", (arc4random() % kUniqueNumberSymbolsCount)]];
    }
    return phoneNumber;
}

@end
