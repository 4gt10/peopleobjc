//
//  MockPersonGenerator.h
//  People
//
//  Created by 4gt10 on 21.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^onCompletion)();

@interface MockPersonGenerator : NSObject

- (void) generatePeopleInBackgroundQueue: (NSUInteger) peopleCount
                             shouldDelay: (BOOL) shouldDelay
                              completion: (onCompletion) completion;

@end
