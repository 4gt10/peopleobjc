//
//  ViewController.m
//  People
//
//  Created by 4gt10 on 21.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "ViewController.h"
#import "PersonCell.h"
#import "AppDelegate.h"
#import "Person+CoreDataProperties.h"
#import "Phone+CoreDataProperties.h"
#import "MockPersonGenerator.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *peopleTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingAIW;
@property (weak, nonatomic) IBOutlet UIButton *addPeopleButton;

@property (nonatomic, copy) NSMutableArray *people;

@end

@implementation ViewController

- (NSMutableArray *)people {
    if (!_people) {
        _people = [[NSMutableArray alloc] init];
    }
    return _people;
}

#pragma mark - VIew controlelr lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Title
    self.title = @"People";
    
    // Table view cells
    self.peopleTableView.estimatedRowHeight = 87; // IB value
    self.peopleTableView.rowHeight = UITableViewAutomaticDimension;
    
    // Fetch data
    [self fetchPeopleAndUpdateUI];
}

#pragma mark - Table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.people.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PersonCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(PersonCell.self)];
    Person *person = self.people[indexPath.row];
    
    // Image
    cell.avatarImageView.image = [UIImage imageNamed:@"avatar_placeholder"];
    cell.avatarImageView.clipsToBounds = YES;
    NSString *avatarURLString = [person.avatarURL copy];
    dispatch_queue_t avatarQueue = dispatch_queue_create("com.arturchernov.avatarQueue", 0);
    dispatch_async(avatarQueue, ^{
        NSURL *avatarURL = [NSURL URLWithString:avatarURLString];
        UIImage *avatarImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:avatarURL]];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.avatarImageView.image = avatarImage;
        });
    });
    
    // Name
    cell.nameLabel.text = person.name;
    
    // Phones
    NSString *phones = @"";
    for (Phone *phone in person.phones) {
        phones = [phones stringByAppendingString:[NSString stringWithFormat:@"%@: %@\n", phone.title, phone.number]];
    }
    cell.phonesLabel.text = phones;
    
    return cell;
}

#pragma mark - Actions

- (IBAction)addMorePeople {
    [self addPeopleAndUpdateUIDelaying:YES];
}

#pragma mark - Helpers

- (void) fetchPeopleAndUpdateUI {
    // Fetch people from DB and generate if there are no ones
    NSManagedObjectContext *context = ((AppDelegate*)[UIApplication sharedApplication].delegate).managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass(Person.self)];
    NSError *error;
    NSArray *people = [context executeFetchRequest:fetchRequest error:&error];
    if (people.count > 0) {
        self.people = [NSMutableArray arrayWithArray:people];
        [self.peopleTableView reloadData];
    } else {
        [self addPeopleAndUpdateUIDelaying:NO];
    }
}

- (void) addPeopleAndUpdateUIDelaying: (BOOL) shouldDelay {
    const NSUInteger kGeneratedPeopleAmount = 10;
    
    // Loading new people in background queue
    MockPersonGenerator *generator = [[MockPersonGenerator alloc] init];
    [generator generatePeopleInBackgroundQueue:kGeneratedPeopleAmount
                                   shouldDelay: shouldDelay
                                    completion:^() {
                                        [self fetchPeopleAndUpdateUI];
                                        [self stopLoadingAnimations];
                                    }
     ];
    
    // Update UI
    [self startLoadingAnimations];
}

- (void) startLoadingAnimations {
    self.loadingAIW.hidden = false;
    [self.loadingAIW startAnimating];
    self.addPeopleButton.hidden = true;
}

- (void) stopLoadingAnimations {
    self.loadingAIW.hidden = true;
    [self.loadingAIW stopAnimating];
    self.addPeopleButton.hidden = false;
}

@end
